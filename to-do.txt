From Assignment 2 Feedback
 - //Use nav, header, section instead of just divs
 - //Hover effects to all links
 - //Vertical spacing on text
 - //Left and right padding in mobile
 - //Revise portfolio to orient toward client
   - //Goal of each site, short description, what you did
   - //Better images

 - //Complete Credentials page which was left unfinished for the prototype.
 - //Flesh out content on portfolio page, although the layout and design is complete. 
 - //Optimise for mobile including drop down navigation bar
   - //Navigation links disappear at this stage due to intended end point functionality. 
 - Complete backend on contact form with client side validation.  
 - //Vertical alignment of skills images

ISSUES TO FIX
 - //portfolio and skills get too small when reduced screen size
 - //nav bar end not visible when shrinking
 - //portfolio horizontal alignment when small
 - //Credentials icon