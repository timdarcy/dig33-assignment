function navCentre(small) {
    if (small.matches) { 
        var item = document.getElementById("navList");
        var item2 = document.getElementById("logoTitle");
        item2.classList.add("responsive");
        console.log("small works");
    } 
}

function navRight(big) {
    if (big.matches) { 
        var item = document.getElementById("navList");
        var item2 = document.getElementById("logoTitle");
        item2.classList.remove("responsive");
        console.log("big works");
    } 
}

var small = window.matchMedia("(max-width: 576px)");
navCentre(small); 
small.addListener(navCentre);

var big = window.matchMedia("(min-width: 576px)");
navRight(big);
big.addListener(navRight);